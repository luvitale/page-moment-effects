<?php
/**
 * Plugin Name: Page Moment Effects
 * Plugin URI: https://www.unesma.net
 * Description: Effects for different page moments.
 * Version: 1.0
 * Author: Luciano Nahuel Vitale
 * Author URI: https://luvitale.net
 */

add_action( 'wp_enqueue_scripts', 'wppme_enqueue_scripts' );
function wppme_enqueue_scripts() {
	wp_enqueue_script('jquery');
  wp_enqueue_script( 'wp-fade-js', plugins_url( "js/fade.js", __FILE__ ) );
}

function shortcode_test_fade() {
  ?>
  <div>
    <?php for ($i = 1; $i < 10; ++$i): ?>
    <div class="fade">Fade In <?=$i?></div>
    <?php endfor ?>
  </div>
  <?php
}
add_shortcode('test_fade', 'shortcode_test_fade');
